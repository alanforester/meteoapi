require 'spec_helper'

describe API::Base do
  include Rack::Test::Methods

  def app
    API::Base
  end

  context 'GET /health' do
    it 'returns health status' do
      get '/health'

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq({status: "OK"}.to_json)
    end
  end

  context 'GET /weather/by_time/:ts' do
    oldTemp = Time.now - 2.hours
    before { create(:temp, val: 24.3, created_at: Time.now - 30.minutes) }
    let(:expected_response) { Temp.select("val", "created_at").first.as_json(:except => :id).to_json }
    let(:not_found_response) { {"error": "Temperature not found"}.to_json }

    it 'returns temp by past time 1 hour' do
      get "/weather/by_time/#{Time.now.to_i}"
      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end

    it 'returns 404 by time past more then 2 hours' do
      get "/weather/by_time/#{oldTemp.getutc.to_i}"
      expect(last_response.status).to eq(404)
      expect(last_response.body).to eq(not_found_response)
    end
  end

  context 'GET /weather/current' do
    before { create(:temp) }
    let(:expected_response) { Temp.select("val", "created_at").first.as_json(:except => :id).to_json }

    it 'returns last temp' do
      get '/weather/current'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end
  end

  
  context 'GET /weather/historical' do
    before { [0..25].each { |t| create(:temp, val: 24.3) } }  
    let(:expected_response) { Temp.select("val", "created_at").all.limit(24).as_json(:except => :id).to_json }

    it 'returns historical temp for 24 hours' do
      get '/weather/historical'

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end
  end

  context 'GET /weather/historical/avg' do
    before {  (0..25).each { |t| create(:temp, val: t, created_at: Time.now - t.hour) } } 
    let(:expected_response) { { avg: "11.5" }.to_json }

    it 'returns average temp for 24 hours' do
      get '/weather/historical/avg'

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end
  end

  context 'GET /weather/historical/max' do
    before {  (0..25).each { |t| create(:temp, val: t, created_at: Time.now - t.hour) } }  
    let(:expected_response) { {max: 23.0}.to_json }

    it 'returns max temp for 24 hours' do
      get '/weather/historical/max'

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end
  end

  context 'GET /weather/historical/min' do
    before {  (0..25).each { |t| create(:temp, val: t, created_at: Time.now - t.hour) } } 
    let(:expected_response) { {min: 0.0 }.to_json }

    it 'returns min temp for 24 hours' do
      get '/weather/historical/min'

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq(expected_response)
    end
  end
end

