ENV['RACK_ENV'] = 'test'
ENV['DATABASE_URL'] = "jdbc:postgresql://#{ENV['POSTGRES_USER']}:#{ENV['POSTGRES_PASSWORD']}@#{ENV['POSTGRES_HOST']}:#{ENV['POSTGRES_PORT']}/#{ENV['POSTGRES_DB']}"

require 'simplecov'
SimpleCov.start

require File.expand_path('../../config/environment', __FILE__)
require 'rspec'
require 'shoulda-matchers'
require 'factory_bot'

require 'database_cleaner/active_record'

require 'faraday'

require 'dotenv'
Dotenv.load


require 'vcr'
VCR.configure do |c|
  c.cassette_library_dir = 'spec/fixtures/vcr_cassettes' #указываем директорию где у нас будут лежать файлы с цепочками запросов
  c.ignore_hosts '127.0.0.1', 'localhost'
  c.hook_into :faraday
  c.debug_logger = $stderr
  c.configure_rspec_metadata!
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.library :active_record
    with.test_framework :rspec
  end
end

RSpec.configure do |config|
  #Shoulda-Matchers
  config.include(Shoulda::Matchers::ActiveModel, type: :model)
  config.include(Shoulda::Matchers::ActiveRecord, type: :model)

  #Factory Bot
  config.include(FactoryBot::Syntax::Methods)
  config.before(:suite) do
    FactoryBot.find_definitions
    DatabaseCleaner.strategy = :transaction
    # DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.around(:each) do
    DatabaseCleaner.clean
  end
end

