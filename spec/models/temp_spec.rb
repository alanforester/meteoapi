ENV["NO_DB"] = "true"
require 'spec_helper'

describe Temp, type: :model do
   it do
      should validate_numericality_of(:val)
   end
end

