class Temp < ActiveRecord::Base
    validates_numericality_of :val

    scope :latest, lambda { order(id: :desc) }
    scope :for24hours, lambda { where("created_at > ?", Time.now - 24.hours)}
    
end

