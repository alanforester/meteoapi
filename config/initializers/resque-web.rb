require 'json'
require 'yaml'
require 'resque'

Resque.redis = Redis.new(:host =>  ENV['REDIS_HOST', :port =>  ENV['REDIS_PORT'])

require 'resque/scheduler'
require 'resque/scheduler/server'

schedule_yml = ENV['RESQUE_SCHEDULE_YML']
if schedule_yml
  Resque.schedule = if File.exist?(schedule_yml)
                      YAML.load_file(schedule_yml)
                    else
                      YAML.load(schedule_yml)
                    end
end

schedule_json = ENV['RESQUE_SCHEDULE_JSON']
if schedule_json
  Resque.schedule = if File.exist?(schedule_json)
                      JSON.parse(File.read(schedule_json))
                    else
                      JSON.parse(schedule_json)
                    end
end

class Putter
  @queue = 'putting'

  def self.perform(*args)
    args.each { |arg| puts arg }
  end
end