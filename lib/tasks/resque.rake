# Resque tasks
require 'resque'
require 'resque/tasks'
require 'resque/scheduler'
require 'resque/scheduler/tasks'
require 'yaml'
require './app/jobs/fetcher'

namespace :resque do
  task :setup do
    Resque.redis = Redis.new(:host =>  ENV['REDIS_HOST'], :port =>  ENV['REDIS_PORT'])
    Resque.logger.level = Logger::DEBUG

    db_config = YAML.load(ERB.new(File.read("./config/database.yml")).result)[ENV['RACK_ENV']]
    ActiveRecord::Base.default_timezone = :utc
    ActiveRecord::Base.establish_connection(db_config)
  end

  task :setup_schedule => :setup do
    Resque.schedule = YAML.load_file('./config/scheduler.yml')
    require 'jobs'
    Resque.before_fork do |job|
        ActiveRecord::Base.establish_connection
    end
  end

  task :scheduler => :setup_schedule

end