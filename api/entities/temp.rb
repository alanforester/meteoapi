require 'grape-swagger-entity'

module API
  module Entities
    class Temp < Grape::Entity
      expose :val
      expose :created_at
    end
  end
end
