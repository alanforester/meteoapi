require 'grape-swagger'

module API
  class Base < Grape::API    
    format :json

    require File.dirname(__FILE__) + '/weather/base'
    mount Weather::Base

    desc 'Return a service status.'
    get :health do
      {status: "OK"} 
    end
   
    add_swagger_documentation \
      :info => {
        :title => "Meteo API"
      },
      # :hide_documentation_path => true,
      :mount_path => "/swagger",
      :markdown => false,
      :add_version => false,
      :base_path => ''
  end
end

