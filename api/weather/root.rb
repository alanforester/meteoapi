module Weather
  class Root < Grape::API
    rescue_from NotFoundError, ActiveRecord::RecordNotFound do |exception|
      Log.instance.log_request(:warn, env, exception)
      error!('Temperature not found', 404)
    end

    rescue_from ActiveRecord::RecordInvalid do |exception|
      Log.instance.log_request(:warn, env, exception)
      error!(exception.message, 409)
    end

    resource :weather do
      require File.dirname(__FILE__) + '/historical'
      mount Weather::Historical

      desc 'Return current weather metrics.'
      get :current do
        temp = Temp.latest.first
        raise NotFoundError if temp.nil?

        present temp, with: API::Entities::Temp
      end

      desc 'Return weather metrics by timestamp.'
      params do
          requires :timestamp, type: Integer, desc: 'Timestamp'
      end
      get '/by_time/:timestamp' do
        tsMax = params[:timestamp].to_i
        tsMin = tsMax - 3600
        
        temp = Temp.latest.where("? < created_at AND created_at < ?  ",Time.at(tsMin).to_datetime, Time.at(tsMax).to_datetime ).first
        raise NotFoundError if temp.nil? 

        present temp, with: API::Entities::Temp   
      end
    end
  end
end

